# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "mysql_exporter/map.jinja" import mysql_exporter with context %}

mysql_exporter-create-user:
  user.present:
    - name: {{ mysql_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

mysql_exporter-create-group:
  group.present:
    - name: {{ mysql_exporter.service_group }}
    - system: True
    - members:
      - {{ mysql_exporter.service_user }}
    - require:
      - user: {{ mysql_exporter.service_user }}

mysql_exporter-bin-dir:
  file.directory:
   - name: {{ mysql_exporter.bin_dir }}
   - makedirs: True

mysql_exporter-dist-dir:
 file.directory:
   - name: {{ mysql_exporter.dist_dir }}
   - makedirs: True

mysql_exporter-version-dir:
 file.directory:
   - name: {{ mysql_exporter.dist_dir }}/mysql_exporter-{{ mysql_exporter.version }}
   - makedirs: True

mysql_exporter-config-dir:
  file.directory:
    - name: {{ mysql_exporter.config_dir }}
    - makedirs: True

mysql_exporter-install-binary:
  archive.extracted:
    - name: {{ mysql_exporter.dist_dir }}/mysql_exporter-{{ mysql_exporter.version }}
    - source: https://github.com/prometheus/mysqld_exporter/releases/download/v{{ mysql_exporter.version }}/mysqld_exporter-{{ mysql_exporter.version }}.{{ grains['kernel'] | lower }}-{{ mysql_exporter.arch }}.tar.gz
    - skip_verify: True
    - options: --strip-components=1
    - user: root
    - group: root
    - enforce_toplevel: False
    - unless:
      - '[[ -f {{ mysql_exporter.dist_dir }}/mysql_exporter-{{ mysql_exporter.version }}/mysql_exporter ]]'
  file.symlink:
    - name: {{ mysql_exporter.bin_dir }}/mysql_exporter
    - target: {{ mysql_exporter.dist_dir }}/mysql_exporter-{{ mysql_exporter.version }}/mysqld_exporter
    - mode: 0755
    - unless:
      - '[[ -f {{ mysql_exporter.bin_dir }}/mysql_exporter ]] && {{ mysql_exporter.bin_dir }}/mysql_exporter -v | grep {{ mysql_exporter.version }}'

mysql_exporter-create-my-cnf:
  file.managed:
    - name: {{ mysql_exporter.config_dir }}/.my.cnf
    - source: salt://mysql_exporter/files/my.cnf.j2
    - template: jinja
    - context:
        mysql_exporter: {{ mysql_exporter }}
    - user: root
    - group: root
    - mode: 644

mysql_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/mysql_exporter.service
    - source: salt://mysql_exporter/files/mysql_exporter.service.j2
    - template: jinja
    - context:
        mysql_exporter: {{ mysql_exporter }}
  service.running:
    - name: mysql_exporter
    - enable: True
    - start: True

mysql_exporter-service-reload:
  service.running:
    - name: mysql_exporter
    - enable: True
    - watch:
      - file: mysql_exporter-install-service

import time

def test_binary_created(File):
    file = File('/usr/bin/mysql_exporter')
    assert file.exists
    assert file.is_symlink
    assert file.user == 'root'
    assert file.group == 'root'

def test_my_cnf_created(File):
    file = File('/etc/mysql_exporter/.my.cnf')
    assert file.exists
    assert file.user == 'root'
    assert file.group == 'root'

def test_service_definition_created(File):
    file = File('/etc/systemd/system/mysql_exporter.service')
    assert file.exists
    assert file.is_file

def test_service_is_running_and_enabled(Service):
    mysql_exporter = Service('mysql_exporter')
    time.sleep(5)
    assert mysql_exporter.is_running
    assert mysql_exporter.is_enabled

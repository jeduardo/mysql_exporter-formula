def test_binary_absent(File):
    file = File('/usr/bin/mysql_exporter')
    assert not file.exists

def test_config_dir_absent(File):
    file = File('/etc/mysql_exporter')
    assert not file.exists

def test_opt_dir_not_present(File):
    file = File('/opt/mysql_exporter/dist')
    assert not file.exists

def test_service_definition_removed(File):
    file = File('/etc/systemd/system/mysql_exporter.service')
    assert not file.exists

def test_service_is_not_running_and_disabled(Service):
    node_exporter = Service('mysql_exporter')
    assert not node_exporter.is_running
    assert not node_exporter.is_enabled
